@extends('layout')

@section('content')
<p>{{ $subscription->id }}</p>
<strong>{{ $subscription }}</strong>
<a href="/cancel/{{ $subscription->id }}" onclick="confirmation(event)">Cancel</a>

<script>
	function confirmation(evt){
		var check = confirm('Are you sure you want to Cancel?');
		if(check){
			return true;
		}
		else{
			evt.preventDefault();
			evt.stopPropagation();
			return false;
		}
	}
</script>
@endsection