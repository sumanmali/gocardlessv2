@extends('layout')

@section('content')
<div class="mt-5">
	<p>{{ $redirectFlow->id}}</p>
	
	<a href="{{ $redirectFlow->redirect_url }}" class="mt-5 btn btn-primary">Proceed to Payment</a>
	<a href="{{ $redirectFlow->redirect_url }}">{{ $redirectFlow->redirect_url }}</a>
</div>
@endsection