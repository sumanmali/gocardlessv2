@extends('layout')

@section('content')
<a href="test/add" class="btn btn-primary mt-5">Add Customer</a>


	<table class="table table-hover mt-5">
		<thead>
			<td>First Name</td>
			<td>Last Name</td>
			<td>Address</td>
			<td>Email</td>
		</thead>
		<tbody>
			@foreach($customers as $customer)
			<tr>
				<td>{{ $customer->given_name }}</td>
				<td>{{ $customer->family_name }}</td>
				<td>{{ $customer->address_line1 }}</td>
				<td>{{ $customer->email }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>



@endsection