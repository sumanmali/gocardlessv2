<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="{{asset('css/app.css')}}">

</head>
<body>
	<div class="container">
		<div class="row">
			@yield('content')
		</div>
	</div>
	
</body>
</html>