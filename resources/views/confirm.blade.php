@extends('layout')

@section('content')
<div class="mt-5">
	<p class="alert alert-info">
		// Save this mandate ID for the next section.
		<strong>Customer: {{ $redirectFlow->links->customer }} </strong><br />
		


		// Display a confirmation page to the customer, telling them their Direct Debit has been
		// set up. You could build your own, or use ours, which shows all the relevant
		// information and is translated into all the languages we support.
		<strong class="mt-5">Confirmation URL: <a href="{{ $redirectFlow->confirmation_url }}" class="btn btn-primary">Confirm</a></strong>
	</p>
</div>
@endsection