<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', 'testController@index');
Route::get('/test/add', 'testController@addCustomer');
Route::get('/success', 'testController@success');
Route::get('/confirm', 'testController@confirm');
Route::get('/firstPayment', 'testController@oneOffPayment');
Route::get('/subscribe', 'testController@subscribe');
Route::get('/subscribe', 'testController@subscribe');
Route::get('/cancel/{id}', 'testController@cancel');

Route::get('/manual', 'testController@manual');