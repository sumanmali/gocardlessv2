<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;


class testController extends Controller
{

	protected $client;

	public function __construct(){
		$this->client = new \GoCardlessPro\Client([
	    // We recommend storing your access token in an
	    // environment variable for security
	    'access_token' => 'sandbox_bl3GBEQKMmVZzfJRK9MvuazkNrWXiHIyiG5PoZQL',
	    // Change me to LIVE when you're ready to go live
	    'environment' => \GoCardlessPro\Environment::SANDBOX
	    ]);
	}

	public function index(){
		$customers = $this->client->customers()->list()->records;
		return view('customers', compact('customers'));
	}

	public function addCustomer(){
		$redirectFlow = $this->client->redirectFlows()->create([
		    "params" => [
		        // This will be shown on the payment pages
		        "description" => "Wine boxes",
		        // Not the access token
		        "session_token" => session()->getId(),
		        "success_redirect_url" => "http://127.0.0.1:8000/success",
		        // Optionally, prefill customer details on the payment page
		        "prefilled_customer" => [
		          "given_name" => "Tim",
		          "family_name" => "Rogers",
		          "email" => "tim@gocardless.com",
		          "address_line1" => "338-346 Goswell Road",
		          "city" => "London",
		          "postal_code" => "EC1V 7LQ"
		        ]
		    ]
		]);
		session()->put('flow_id', $redirectFlow->id);
		echo "here comes idSS";

		$value=session()->put('reId',$redirectFlow->id);
		return view('redirect', compact('redirectFlow'));
	}


	public function success(){
		$redirectFlow = $this->client->redirectFlows()->complete(
		    session()->get('flow_id'), //The redirect flow ID from above.
		    ["params" => ["session_token" => session()->getId()]]
		);

        print("Mandate: " . $redirectFlow->links->mandate . "<br />");
// Save this mandate ID for the next section.
        print("Customer: " . $redirectFlow->links->customer . "<br />");

// Display a confirmation page to the customer, telling them their Direct Debit has been
// set up. You could build your own, or use ours, which shows all the relevant
// information and is translated into all the languages we support.
        print("Confirmation URL: " . $redirectFlow->confirmation_url . "<br />");


		session()->put('mandate', $redirectFlow->links->mandate);

		return view('confirm', compact('redirectFlow'));
	}

	public function subscribe(){

		$subscription = $this->client->subscriptions()->create([
		  "params" => [
		      "amount" => 1500, // 15 GBP in pence
		      "currency" => "GBP",
		      "interval_unit" => "monthly",
		      "day_of_month" => "5",
		      "links" => [
		          "mandate" => session()->get('mandate')

		                       // Mandate ID from the last section
		      ],
		      "metadata" => [
		          "subscription_number" => "ABC1234"
		      ]
		  ],
		  "headers" => [
		      "Idempotency-Key" => "random_subscription_specific_string"
		  ]
		]);

	print("ID: " . $subscription->id);

		return view('subscribe', compact('subscription'));
	}

	public function cancel($id){
		$subscription = $this->client->subscriptions()->cancel($id);
		return redirect('/test');
	}


	public function manual(){
		 $payment = $this->client->payments()->create([
			  "params" => [
			      "amount" => 10000, // 10 GBP in pence
			      "currency" => "GBP",
			      "links" => [
			          "mandate" => session()->get('mandate')
			                       // The mandate ID from last section
			      ],
			      // Almost all resources in the API let you store custom metadata,
			      // which you can retrieve later
			      "metadata" => [
			          "invoice_number" => session()->getId()
			      ]
			  ],
			  "headers" => [
			      "Idempotency-Key" => session()->getId()
			  ]
			]);

		 dd($payment->id);

		 return redirect('/manual', compact('payment'));
	}

	public function oneOffPayment(){
        $payment = $this->client->payments()->create([
            "params" => [
                "amount" => 999, // 10 GBP in pence
                "currency" => "GBP",
                "links" => [
                    "mandate" => "MD0005KFP96W61"
                    // The mandate ID from last section
                ],
                // Almost all resources in the API let you store custom metadata,
                // which you can retrieve later
                "metadata" => [
                    "invoice_number" => "002"
                ]
            ],
            "headers" => [
                "Idempotency-Key" => "random_payment_specific_string"
            ]
        ]);

// Keep hold of this payment ID - we'll use it in a minute
// It should look like "PM000260X9VKF4"
        print("ID: " . $payment->id);
    }

    public function subscribe2(){
        $subscription = $this->client->subscriptions()->create([
            "params" => [
                "amount" => 1500, // 15 GBP in pence
                "currency" => "GBP",
                "interval_unit" => "monthly",
                "day_of_month" => "5",
                "links" => [
                    "mandate" => "MD0005KFP96W61"
                    // Mandate ID from the last section
                ],
                "metadata" => [
                    "subscription_number" => "ABC1234"
                ]
            ],
            "headers" => [
                "Idempotency-Key" => "random_subscription_specific_string"
            ]
        ]);

// Keep hold of this subscription ID - we'll use it in a minute.
// It should look a bit like "SB00003GKMHFFY"
        print("ID: " . $subscription);
    }


}
